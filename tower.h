#ifndef TOWER_H
#define TOWER_H
#include <QVector>
#include <card.h>


class Tower
{
private:
    QVector<Card> cards;

public:
    Tower();
    void addCard(Card *card);
    void removeCard(int index);
    void flipCard(Card *card);
    void setTower();
    bool checkIfAvailable(Card *card);
};

#endif // TOWER_H
