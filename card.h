#ifndef CARD_H
#define CARD_H

enum cardType { Spade, Heart, Diamond, Club };
enum cardState { Revealed, Hidden };
enum cardColor { Red, Black };

class Card
{
private:
    int Number;
    cardType Type;
    cardState State;
    cardColor Color;

public:
    Card();

    // Get Functions
    int GetNumber();
    cardType GetType();
    cardState GetState();
    cardColor GetColor();

    // Set Functions
    void SetNumber(int N);
    void SetType(cardType T);
    void SetState(cardState S);
    void SetColor(cardColor C);
    void SetCard(int n, cardType t, cardState s);
};

#endif // CARD_H
