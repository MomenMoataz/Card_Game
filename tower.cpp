#include "tower.h"

Tower::Tower()
{

}

void Tower::addCard(Card *card)
{
    cards.append(*card);
}

void Tower::removeCard(int index)
{
    cards.remove(index);
}

void Tower::flipCard(Card *card)
{
    cardState state = card->GetState();
    if(state = Revealed) card->SetState(Hidden);
    else if(state = Hidden) card->SetState(Revealed);
}

bool Tower::checkIfAvailable(Card *card)
{
    int number = card->GetNumber();
    cardColor color = card->GetColor();
    Card lastCard = cards.at(cards.size());
    if(lastCard.GetNumber() == number - 1 && lastCard.GetColor() != color)
        return true;
    else
        return false;
}
