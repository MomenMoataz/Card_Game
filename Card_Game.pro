#-------------------------------------------------
#
# Project created by QtCreator 2016-12-05T19:24:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Card_Game
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    card.cpp \
    tower.cpp

HEADERS  += mainwindow.h \
    card.h \
    tower.h

FORMS    += mainwindow.ui
